from __future__ import print_function

import keras
from keras.datasets import fashion_mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau
from keras.preprocessing.image import ImageDataGenerator
from keras import backend as K
import numpy as np
import os

batch_size = 300
num_classes = 10
epochs = 25

img_rows, img_cols = 28, 28

(x_train, y_train), (x_test, y_test) = fashion_mnist.load_data()

if K.image_data_format() == 'channels_first':
	x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
	x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
	input_shape = (1, img_rows, img_cols)
else:
	x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
	x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
	input_shape = (img_rows, img_cols, 1)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3), activation = 'relu', input_shape = input_shape))
model.add(Conv2D(64, (3, 3), activation = 'relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.1))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.1))
model.add(Dense(num_classes, activation='softmax'))

# Save the checkpoint in the /output folder
filepath = "best.hdf5"

if os.path.exists(filepath) == True:
	model.load_weights(filepath)



# Keep only a single checkpoint, the best over test accuracy.
checkpoint = ModelCheckpoint(filepath,
                            monitor='val_acc',
                            verbose=1,
                            save_best_only=True,
                            mode='max')
#ReduceLROnPlateau
lr = ReduceLROnPlateau(monitor='val_loss',
								factor=0.1,
								patience=10,
								verbose=0,
								mode='auto',
								min_delta=0.0001,
								cooldown=0,
								min_lr=0)

model.compile(loss=keras.losses.categorical_crossentropy, optimizer=keras.optimizers.Adam(), metrics=['accuracy'])

score = model.evaluate(x_test, y_test, verbose = 0)

print('Test loss:', score[0])
print('Test accuracy:', score[1])

print(model.summary())

image_generator = ImageDataGenerator(zoom_range = 0.1,
                   					horizontal_flip = True)

#model.fit(x_train, y_train, batch_size=batch_size, epochs=5, verbose=1, validation_data=(x_test, y_test), callbacks=[checkpoint,lr])

# Train model on dataset
#model.fit_generator(image_generator.flow(x_train, y_train, batch_size=batch_size),
#										steps_per_epoch=int(np.ceil(x_train.shape[0] / float(250))),
#										epochs=15,
#										validation_data=(x_test,y_test),
#										callbacks=[checkpoint,lr])

#model.fit(x_train, y_train, batch_size=batch_size, epochs=25, verbose=1, validation_data=(x_test, y_test), callbacks=[checkpoint,lr])

score = model.evaluate(x_test, y_test, verbose = 0)

print('Test loss:', score[0])
print('Test accuracy:', score[1])
